프로젝트 구성 

- main.cpp 에 calibration test 관련 모듈이 모두 작성되어 있습니다. 

- example.cpp 는 초기 코드 작성 당시 참고했던 예시로 조만간 삭제할 예정입니다.

내부 모듈 주요 설명 

1) check_calibrate 

- glob 함수: 이미지 저장 path를 받아 해당 디렉토리에 모든 이미지파일을 vector<String> images 에 저장합니다. 

- images 변수를 받아 path에 저장된 모든 이미지를 사용하여 calibration을 진행합니다.

2) colcheckerr/rowcheckerr 

- calibration 과정에서 계산된 corner 들에 대한 undistortion error를 계산합니다. 
- (module) 각 column/row 양 끝점을 잇는 직선과 그 사이에 위치한 점들의 거리를 계산하여 평균합니다.

3) calculate_error_inline - 2-(module) 과정을 수행합니다.

추후 변경 예정 사항 

- 사전에 계산된 (이미지, corner, intrinsic parameter) set으로 undistortion error를 계산할 수 있는 모듈 추가
