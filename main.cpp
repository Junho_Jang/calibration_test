#pragma warning(disable:4996);
#include <opencv2/opencv.hpp>


#include <opencv2/calib3d/calib3d_c.h>


#include <vector>
#include <iostream>

using namespace cv;
using namespace std;


#if(1)
#define Rownumber 4
#define Colnumber 10 
#define gridsize 5
#define path "C:\\0604_small"
//#define path "C:\\0608_1_0"
#else
#define Rownumber 5 
#define Colnumber 9 
#define gridsize 10 
#define path "C:\\0604_big_bmp"

#endif

#define Resolutionwidth 1280
#define Resolutionheight 1024


/*----------------------------------------------------------------------------------/
					saved intrinsic parameter
-----------------------------------------------------------------------------------*/
float S_0604_cmat[] = { 839.7420948167644, 0, 625.0366087273453, 0, 837.584322745962, 523.095020931849, 0, 0, 1 };
float S_0604_dcoef[] = { -0.4813510396753682, 0.2734985885941599, 0.001683812643074363, -0.001264410180442152, -0.0830374108416119 };
float B_0604dcoef[] = { -0.4550874530152887, 0.24778205936299, 0.004101299049216877, 0.003063411892917556, -0.07819788759032206 };
float B_0604_cmat[] = {829.7721293530367, 0, 602.2924557852104, 0, 826.3019670023873, 527.6674529407992, 0, 0, 1};



int check_calibrate(vector<string> images, Size patten, int grid);
void colcheckerr(string images, Mat cMat, Mat distC, vector <Point2f> corner, Size Resolution, Size pattern);
void rowcheckerr(string images, Mat cMat, Mat distC, vector <Point2f> corner, Size Resolution, Size pattern);
float calculate_error_inline(Point2f min, Point2f max, vector<Point2f>all);
void showundistimg(string  name, Mat cMat, Mat distC);
void contrastup(void* src, Mat &dst, float alpha);
Mat calcGrayHist(const Mat&img);
Mat getGrayHistImage(const Mat& hist);
int main(void) {
	vector<String> images;
	Size patternsize(Colnumber, Rownumber);
	glob(path, images, 0);
	check_calibrate(images,patternsize,gridsize);
#if(0) // only check undistrted images with save intrinsic parameter

	Mat ScMat(3, 3, CV_32F, SmallcamMatrix);
	Mat BcMAt(3, 3, CV_32F, BigcamMatrix);
	//Mat dist(1, 5, CV_32F, SmallDistcoeff);
	Mat dist(1, 5, CV_32F, BigDistcoeff);
	glob("C:\\0604_small", images, 0);
	//glob("C:\\0604_big_bmp", images, 0);
	for (int i = 0; i < images.size(); i++) {
		Mat img = imread(images[i]);
		Mat imgOut;
		undistort(img, imgOut, ScMat, dist);

		imshow("Name", imgOut);
		waitKey();

	}
#endif

	return 0;
}
int check_calibrate(vector<string> images,Size patten,int grid) {

	//Size patten(Colnumber, Rownumber);
	int Rownum = patten.height;
	int Colnum = patten.width;
	int res=0;
	if (images.size() == 0) {
		cout << "Not detected image (path)" << endl;
		res = -1;
	}
	cout << "Number of images " << images.size() << endl;

	vector<Point3f> objp;
	for (int i = 0; i < Rownum; i++) {
		for (int j = 0; j < Colnum; j++) {
			objp.push_back(Point3f(j * grid, i * grid, 0));
		}
	}

	vector<Point2f> singlecorner;
	vector<vector<Point2f>> corner_all;
	vector<vector<Point3f>> objp_all;
	vector<int> nocornerindex;
	int numcount = 0;
	for (int i = 0; i < images.size(); i++) {
		cout << i + 1 << "th image : " << images[i] << endl;
		Mat img = imread(images[i], IMREAD_GRAYSCALE);
		//contrastup((void*)&img, img, 0.5);
		//imshow("name", img1);
		//waitKey();
		cout << "Read images" << endl;
		//int patternfound = findChessboardCorners(img, patten, array1,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
		int patternfound = findChessboardCorners(img, patten, singlecorner, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
		//int patternfound = findChessboardCorners(img, patten, array1, CV_CALIB_CB_ADAPTIVE_THRESH |CV_CALIB_CB_FAST_CHECK);
		//int patternfound = findChessboardCorners(img, patten, array1, CV_CALIB_CB_ADAPTIVE_THRESH |CV_CALIB_CB_NORMALIZE_IMAGE);

		// 마지막 매크로 변화에 따라 값이 바뀜 -- 체크
		if (patternfound) {
			numcount++;
			nocornerindex.push_back(1);
			TermCriteria criteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.001);
			cout << "Subpix - ";
			cornerSubPix(img, singlecorner, patten, Size(-1, -1), criteria);
			cout << "Draw - ";
			drawChessboardCorners(img, patten, singlecorner, patternfound);

			objp_all.push_back(objp);
			corner_all.push_back(singlecorner);
			cout << i + 1 << "th image success" << endl;
		}
		else {
			nocornerindex.push_back(0);
			cout << i + 1 << "th image failed" << endl;
		}
	}

	destroyAllWindows();
	if (numcount == 0) {
		cout << "No images are detected " << endl;
		return -1;
	}

	Mat cMat; // = Mat::zeros(3, 3, CV_32F);
	Mat distC; // = Mat::zeros(1, 5, CV_32F);
	Mat Rvec, Tvec;
	double err = 0.;
	Size imResol(Resolutionwidth, Resolutionheight);
	TermCriteria criteria(2 | 1, 30, 0.001);
	int flags = 0;

	cout << "Start Calibration" << endl;
	err = calibrateCamera(objp_all, corner_all, imResol, cMat, distC, Rvec, Tvec, flags | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5);

	cout << "Result " << endl;
	cout << "Camera matrix" << endl;
	cout << cMat << endl;
	cout << "Distortion coefficient " << endl;
	cout << distC << endl;

	 

	double fx = cMat.at<double>(0, 0);
	double fy = cMat.at<double>(1, 1);
	double cx = cMat.at<double>(0, 2);
	double cy = cMat.at<double>(1, 2);
	double a = 0;
	int cnt = 0;
	for (int i = 0; i < images.size(); i++) {
		if (nocornerindex[i] == 0) {
			continue;
		}
		rowcheckerr(images[i], cMat, distC, corner_all[cnt],imResol,patten);
		colcheckerr(images[i], cMat, distC, corner_all[cnt], imResol, patten);
		cnt++;

	}

}
void colcheckerr(string images, Mat cMat, Mat distC, vector <Point2f> corner, Size Resolution, Size pattern) {
	vector <Point2f> udcorner;
	vector <Point2f> checkudcorner;
	Mat img = imread(images);
	Mat imgOut;
	Point2f MinP;
	Point2f MaxP;

	undistortPoints(corner, udcorner, cMat, distC);
	undistort(img, imgOut, cMat, distC);

	int idx = 0;
	double fx = cMat.at<double>(0, 0);
	double fy = cMat.at<double>(1, 1);
	double cx = cMat.at<double>(0, 2);
	double cy = cMat.at<double>(1, 2);

	// normalized point --> unnormalized point

	int Rownum = pattern.height;
	int Colnum = pattern.width;
	int ResolWidth = Resolution.width;
	int ResolHeight = Resolution.height;

	for (int k = 0; k < Colnum; k++) {//colnum
		for (int j = 0; j < Rownum; j++) {
			idx = j * Colnum + k;
	
			udcorner[idx].x = udcorner[idx].x*fx + cx;
			udcorner[idx].y = udcorner[idx].y*fy + cy;

			if (udcorner[idx].x<0 || udcorner[idx].x>ResolWidth || udcorner[idx].y<0 || udcorner[idx].y>ResolHeight) {
				checkudcorner.push_back((Point2f)(0, 0));
			}
			else {
				checkudcorner.push_back(udcorner[idx]);
			}
			
		} 
	}


	float *e1 = (float*)malloc(sizeof(float)*Colnum);
	//float e1[Rownum];
	idx = -1;
	for (int k = 0; k < Colnum; k++) {
		int norow = 0;
		int flag = 0;
		vector<Point2f> P_inline;

		for (int j = 0; j < Rownum; j++) {			
			idx++;
			if (checkudcorner[idx].x == 0 || checkudcorner[idx].y == 0) {
				flag = 0;
				norow++;
				continue;
			}
			else {
				if (flag == 0) {
					MinP = checkudcorner[idx];
					P_inline.push_back(MinP);
					flag++;
					continue;
				}
			}
			if (flag == 1) {
				MaxP = checkudcorner[idx];
				P_inline.push_back(MaxP);
			}


		};
	
		if (Colnum - norow <= 2) {
			cout << "# of point < 2" << endl;
		}
		else {
			 
			//imshow("Name", imgOut);
			//line(imgOut, MinP, MaxP, Scalar(0, 0, 255), 10);
			//waitKey();
			e1[k] = calculate_error_inline(MinP, MaxP, P_inline);
		}


	}

	float ave = 0;
	for (int k = 0; k < Rownum; k++) {
		ave += e1[k];
	}
	ave /= Rownum;
	cout << "Column - Averaged error of " << images << " : " << ave << endl;

}
void rowcheckerr(string images, Mat cMat, Mat distC, vector <Point2f> corner, Size Resolution, Size pattern) {
	{
		vector <Point2f> udcorner;
		vector <Point2f> checkudcorner;
		Mat img = imread(images);
		Mat imgOut;
		Point2f MinP;
		Point2f MaxP;

		undistort(img, imgOut, cMat, distC);
		undistortPoints(corner, udcorner, cMat, distC);

		int idx = 0;
		double fx = cMat.at<double>(0, 0);
		double fy = cMat.at<double>(1, 1);
		double cx = cMat.at<double>(0, 2);
		double cy = cMat.at<double>(1, 2);

		// normalized point --> unnormalized point

		int Rownum = pattern.height;
		int Colnum = pattern.width;
		int ResolWidth = Resolution.width;
		int ResolHeight = Resolution.height;
		for (int k = 0; k < Rownum; k++) {
			for (int j = 0; j < Colnum; j++) {
				idx = k * Colnum + j;

				udcorner[idx].x = udcorner[idx].x*fx + cx;
				udcorner[idx].y = udcorner[idx].y*fy + cy;
				
				if (udcorner[j].x<0 || udcorner[j].x>ResolWidth || udcorner[j].y<0 || udcorner[j].y>ResolHeight) {
					checkudcorner.push_back((Point2f)(0, 0));
				}
				else {
					checkudcorner.push_back(udcorner[idx]);
				}
			}
		}


		float *e1 = (float*)malloc(sizeof(float)*Rownum);
		for (int k = 0; k < Rownum; k++) {
			int norow = 0;
			int flag = 0;
			vector<Point2f> P_inline;
			for (int j = 0; j < Colnum; j++) {
				idx = k * Colnum + j;
				if (checkudcorner[idx].x == 0 || checkudcorner[idx].y == 0) {
					flag = 0;
					norow++;
					continue;
				}
				else {
					if (flag == 0) {
						MinP = checkudcorner[idx];
						P_inline.push_back(MinP);
						flag++;
						continue;
					}
				}
				if (flag == 1) {
					MaxP = checkudcorner[idx];
					P_inline.push_back(MaxP);
				}
			}

			if (Colnum - norow <= 2) {
				cout << "# of point < 2" << endl;
			}
			else {
				line(imgOut, MinP, MaxP, Scalar(0, 0, 255), 3);
				e1[k]= calculate_error_inline(MinP, MaxP, P_inline);
			}
		}
		float ave = 0;
		for (int k = 0; k < Rownum; k++) {
			ave += e1[k];
		}
		ave /= Rownum;
		cout << "Averaged error of " << images << " : " << ave << endl;

	}

}
float calculate_error_inline(Point2f min,Point2f max,vector<Point2f>all) {
	Point2f r1(max.x - min.x, max.y - min.y);
	float magr1 = sqrt(r1.x*r1.x + r1.y*r1.y);
	Point2f r1n(r1.x / magr1,r1.y / magr1);
	float err = 0;

	for (int i = 0; i < all.size(); i++) {
		Point2f r2(all[i].x - min.x, all[i].y - min.y);
		float r1cos = r2.x*r1n.x +r2.y*r1n.y;
		float length = sqrt(fabs(r2.x*r2.x+r2.y*r2.y - r1cos * r1cos));
		err += fabs(length); // type of error can be changed later
	}
	return err;

}
void showundistimg(string  name, Mat cMat, Mat distC) {
	Mat img = imread(name);
	Mat imgOut;
	undistort(img, imgOut, cMat, distC);

	imshow("Name", imgOut);
	waitKey();
}
void contrastup(void* src, Mat &dst, float alpha) {
	Mat src1 = *(Mat*)src;
	Mat dst1 = src1 + (src1 - 128)*alpha;
	dst = dst1;
}


Mat calcGrayHist(const Mat&img) {
	Mat hist;
	int channels[] = { 0 };
	int dims = 1;
	const int histSize[] = { 256 };
	float graylevel[] = { 0,256 };
	const float* ranges[] = { graylevel };
	calcHist(&img, 1, { 0 }, noArray(), hist, 1, histSize, ranges, true, false);

	return hist;
}
Mat getGrayHistImage(const Mat& hist) {
	double max;
	minMaxLoc(hist, 0, &max);
	Mat imgHist(100, 256, CV_8UC1, Scalar(256));
	for (int i = 0; i < 256; i++) {
		line(imgHist, Point(i, 100), Point(i, 100 - cvRound(hist.at<float>(1, 0) * 100. / max)), Scalar(0));
	}
	return imgHist;
}
